> 🔍 Pending review.

# 👁‍🗨 Visual System

An overview of the human visual system.

> Check out the [psychophysics overview](../../Psychophysics.md) first!

[[_TOC_]]

## Key Points

* __The visual system turns light into electro-chemical signals which are then processed by the eyes and the brain.__
* __Our eyes filter light and focus it onto photoreceptors in their retinae.__
	* As we age, our vision becomes more yellow and our range of focus shrinks.
* __Photoreceptors in the retina turn the resulting image into an electro-chemical signal.__
	* We have 2 main types of photoreceptors:
		* Rods, which are the main contributors to vision under dim illumination.
		* Cones, which are the main contributors to vision under bright illumination and [color vision](../../Perceptual Models/Color/Color.md).
			* Humans have 3 cone types: long-wavelength (L), medium-wavelength (M), and short-wavelength (S) sensitive.
				* The relative populations of L : M : S cones are approximately 40 : 20 : 1.
				* Missing or anomalous cone types cause variations of color blindness.
* __The fovea is the spot in the retina where visual acuity is highest.__
	* It's responsible for the ~2° of visual angle around the center of each eye's field of vision.
	* It's filled almost exclusively with cones.
* __The optic disc is the spot in the eye where the optic nerve forms and exits to the brain.__
	* This area creates a blind spot due to the way the neurons in the retina are arranged.
* __The brain doesn't receive a one-to-one image seen by each eye.__

## Overview

### Stimulus

The visual system is stimulated by __light__, a wave that travels through the electromagnetic field.

It's specifically stimulated by light in the visible spectrum which has wavelengths between ~400 and ~700 nm.

> ![The electromagnetic spectrum split into named segments from shortest wavelength (gamma radiation) to longest wavelength (long radio waves).](EM_spectrumrevised.png)
>
> The electromagnetic spectrum[^electromagnetic_spectrum].

### Eye

__Eyes__ are organs in the visual system which focus light onto arrangements of photosensitive cells that turn light into electro-chemical signals.

They can be loosely compared to cameras which use a camera lens to focus an image of the visual world onto an image sensor.

> ![Diagram of the human eye. Displays a simplified illustration of the human eye and its retina.](Human Eye Diagram.png)
>
> Diagram of the human eye[^reconstructing_the_retina].

#### Cornea

Light first passes through the __cornea__, the transparent outer surface of the front of the eye. The cornea helps the eye focus incoming light.

#### Aqueous Humor

The space immediately past the cornea is filled with __aqueous humor__ which is primarily water.

#### Iris & Pupil

The __iris__ is a sphincter muscle which controls the size of its opening known as the __pupil__.

Eye color is determined by the distribution of melanin, a pigment, throughout the iris.

The size of the pupil determines the amount of light that passes through. It's mostly determined by the overall level of illumination but it can also vary due to non-visual phenomena such as arousal.

#### Lens

Alongside the cornea, the __lens__ helps the eye focus incoming light.

The shape of the lens can be adjusted by ciliary muscles to let us focus on objects at various distances in a process known as accommodation. The lens also absorbs and scatters short-wavelength light (blue and violet).

As we age, the lens becomes harder and generally loses its flexibility around age 45-50 which prevents us from focusing on close objects. At this point, most people begin using reading glasses or bifocals.

The amount of absorption and scattering increases as well, causing the lens to yellow with age.

#### Vitreous Humor

Most of the space in the eye past the lens is filled with __vitreous humor__ which is a gelatin-like substance.

#### Retina

The cornea and lens focus light on the __retina__, a thin layer of tissue (approximately the thickness of tissue paper) located near the back of the eye.

The retina contains layers of neurons which:

1. Turn light into electro-chemical signals (__photoreceptors__).
1. Process these signals and transmit them to the brain.

The photoreceptors in the human eye are divided into __rods__ and __cones__. These names are a bit misleading: while rods tend to be long and slender in shape, cones can be either conical or rod-like in shape.

Rods are the main contributors to vision under dim illumination such as moonlight. They are sensitive enough to detect individual photons but become saturated at illumination levels similar to those at around dawn.

Cones are the main contributors to vision under bright illumination such as daylight. They are much less sensitive to light than rods but respond to changes in illumination more quickly.

Human eyes have three cone types: __long-wavelength (L)__, __medium-wavelength (M)__, and __short-wavelength (S)__ sensitive types. Each cone type varies in sensitivity to the electromagnetic spectrum and contributes to [color vision](../../Perceptual Models/Color/Color.md). The relative populations of L : M : S cones are approximately 40 : 20 : 1.

> ![Light absorption spectra of photoreceptor pigments in the human eye. Each curve is shaped like a bell with the short wavelength side elevated above the long wavelength side. The S, M, and L cone pigments have peaks at 440 nm, 530 nm, and 565 nm respectively. The rod pigment has a peak at 500 nm.](Human Eye Photoreceptors Light Absorption Spectra.png)
>
> Light absorption spectra of the photoreceptor pigments for the three cone types and one rod type in a human eye with typical vision[^neuroscience_figure_10.12].

These cone types can either be normal, anomalous (having a non-typical spectral sensitivity), or missing/non-functional. This means there are 3<sup>3</sup> = 27 possible combinations of the 3 cone types, with 26 combinations causing variations of color blindness.

The photoreceptors are attached to a "grid" of neurons which process the photoreceptor signals and transmit them to the brain.

In the "vertical" pathway, signals from photoreceptors pass through __bipolar cells__ and then through __ganglion cells__ which eventually connect to the brain.

In the "lateral" pathway, signals are passed across groups of photoreceptors and bipolar cells through __horizontal cells__ while signals are passed across groups of bipolar cells and ganglion cells through __amacrine cells__.

These neurons are layered such that light entering the eye first passes through the transmission and processing layers before reaching the photoreceptors.

> This has little impact on visual performance since these non-photoreceptor cells are mostly transparent and they aren't perceived because of their fixed positions relative to the photoreceptors.
>
> It also lets the large amount of nutrients required and waste created by photoreceptors to be processed closer to the back of the eye where the retinal pigment epithelium (RPE) is located.
>
> The RPE also absorbs any light that isn't absorbed by the photoreceptors, preventing uncaptured light from scattering through the retina and reducing the sharpness and contrast of the perceived image.

The most important area on the retina is the __fovea__, a small region at the center of the retina where visual acuity is the highest. It accounts for the ~2° of visual angle around the center of the eye's field of vision.

Unlike other areas of the retina which are primarily filled with rods with only some cones, the fovea is filled almost only with cones. These cones are packed so tightly that they take on a rod-like shape instead of the usual conical appearance of cones outside the fovea.

The fovea is covered by a filter known as the __macula__ which protects the fovea from intense short-wavelength light (blue and violet). Unlike the lens, it doesn't become more yellow with age.

Another area of note is the __optic disc__ which is a small, circular area where the previously mentioned ganglion cells bundle together to form the optic nerve. In order to exit the eye, the optic nerve must pass through space where photoreceptors are normally located. As a result, the optic disc has no photoreceptors, creating a blind spot in the eye's field of vision.

> ![Photoreceptor distribution in the retina of the human eye as a function of degrees from the fovea. Rod density steadily increases from the edges of the retina to the fovea until about 20 degrees of the fovea where it sharply drops off. Cone density is consistently low from the edges of the retina to the fovea until a sharp spike at 0 degrees. The optic disc is around 10 to 20 degrees to the side of the retina near the nose.](Human Eye Photoreceptor Distribution.png)
>
> Photoreceptor distribution in the retina of the human eye as a function of degrees from the fovea[^neuroscience_figure_10.10].

### Optic Nerve

The __optic nerve__ transmits signals from the eye to the brain.

It consists of ~1 million fibers carrying information generated by ~130 million photoreceptors, indicating that the brain doesn't receive a one-to-one image seen by each eye.

Instead, the individual signals from photoreceptors are pooled into a smaller set of signals by the "grid" of neurons that process and transmit the signals afterwards.

## Further Learning

* [_Color Appearance Models (3rd Edition)_](https://doi.org/10.1002/9781118653128) by Mark D. Fairchild.
	* Moderately technical overview of the visual system and very technical overview of color appearance models.
	* This article is a simplified version of the visual system summary in this textbook!
* [_Principles of Neural Science (6th Edition)_](https://en.wikipedia.org/wiki/Principles_of_Neural_Science) by Eric R. Kandel et al.
	* Very technical overview of all sensory systems and modern neuroscience.
	* L, M, and S cones are mistakenly called red, green, and blue cones respectively in _Chapter 17, Sensory Coding_ but are correctly named in _Chapter 22, Low-Level Visual Processing: The Retina_.

---

[^electromagnetic_spectrum]: [Electromagnetic spectrum diagram](https://commons.wikimedia.org/wiki/File:EM_spectrumrevised.png) from Wikipedia.

[^reconstructing_the_retina]: [_Reconstructing the Retina_](https://www.nature.com/articles/d41586-018-06111-y) by David Holmes in Nature.

[^neuroscience_figure_10.12]: Figure 10.12 from [_Neuroscience (3rd Edition)_](https://doi.org/10.1212/01.WNL.0000154473.43364.47) by Dale Purves et al.

[^neuroscience_figure_10.10]: Figure 10.10 from [_Neuroscience (3rd Edition)_](https://doi.org/10.1212/01.WNL.0000154473.43364.47) by Dale Purves et al.
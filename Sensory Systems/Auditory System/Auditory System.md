> 🚧 Under construction.

# 🎼 Auditory System

An overview of the human auditory system.

> Check out the [psychophysics overview](../../Psychophysics.md) first!

[[_TOC_]]

## Key Points

?

## Overview

_Insert basic reception, transduction, and processing overview._

* _Chain:_
	* _Acoustic wave_
	* _Outer ear_
		* _Pinna/auricle_ _→ auditory canal_
	* _Middle ear_
		* _Tympanic membrane (ear drum)_
		* _Ossicles_
			* _Malleus (hammer) → incus (anvil) → stapes (stirrup)_
	* _Inner ear_
		* _Cochlea_
			* _Basilar membrane does a mechanical Fourier transform!_
	* _Auditory nerve_
	* _Brain_

### Stimulus

_Acoustic waves (normally, other stuff if you have synesthesia or are under the influence)._

## Further Learning

* [_Principles of Neural Science (6th Edition)_](https://en.wikipedia.org/wiki/Principles_of_Neural_Science) by Eric R. Kandel, James H. Schwartz, and Thomas M. Jessell.
	* _Extremely technical overview of all sensory systems and modern neuroscience._

> 🚧 Under construction.

# ♨️ Somatosensory System

An overview of the human somatosensory system.

> Check out the [psychophysics overview](../../Psychophysics.md) first!

[[_TOC_]]

## Key Points

?

## Overview

_Insert basic reception, transduction, and processing overview._

_Encompasses a lot of senses (pressure, temperature, etc.). Probably need a chain for each sense._

### Stimulus

?

## Further Learning

* [_Principles of Neural Science (6th Edition)_](https://en.wikipedia.org/wiki/Principles_of_Neural_Science) by Eric R. Kandel, James H. Schwartz, and Thomas M. Jessell.
	* _Extremely technical overview of all sensory systems and modern neuroscience._

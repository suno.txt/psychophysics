> 🚧 Under construction.z

# 📍 Sound Location

An overview of perceptual models for human sound location perception.

> Check out the [psychophysics overview](../../Psychophysics.md) first!

[[_TOC_]]

## Key Points

?

## Overview

_Auditory cues. Cone of confusion._

_Head-related transfer functions (HRTFs). Room transfer functions (RTFs)._

## Further Learning

* _Head-related transfer functions (HRTFs) and room transfer functions paper(s)_

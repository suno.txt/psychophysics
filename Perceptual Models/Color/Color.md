> 🔍 Pending review.

# 🎨 Color

An overview of perceptual models for human color vision.

> Check out the [psychophysics overview](../../Psychophysics.md) first!

[[_TOC_]]

## Key Points

* __Cone photoreceptors in the [visual system](../../Sensory Systems/Visual System/Visual System.md) are the main contributors to color vision.__
* __Colors are a psychological phenomena, not a physical property.__
* __Colors don’t have unique spectral power distributions.__
	* This is a consequence of compressing the continuous electromagnetic spectrum (infinite dimensions) into signals from ≤ 3 cone types (≤ 3 dimensions).
	* Different spectral power distributions with the same perceived color are called metamers.
* __Different viewing conditions can cause the same object to have different color appearances.__
	* Viewing conditions can include physical and psychological factors.
* __Color systems and models start from specifying color appearances under one set of viewing conditions to predicting color appearances under a wide range of viewing conditions.__
	* Color naming systems
		* Name arbitrarily-chosen color appearances under no well-defined or just one set of viewing conditions.
		* E.g. Pantone color matching system.
	* Color order systems
		* Order systematically-chosen color appearances under one set of viewing conditions.
		* E.g. CIE standard observers, CMYK-based and RGB-based color systems, Munsell color system, Swedish Natural Color System (NCS).
	* Color appearance models
		* Predict color appearances under a wide range of viewing conditions.
		* E.g. CIELAB, CIELUV, CIECAM97s, CIECAM02, CAM16, IPT, IC<sub>T</sub>C<sub>P</sub>.
	* Image appearance models
		* Predict color appearances under a wider range of viewing conditions than color appearance models.
		* E.g. S-CIELAB, iCAM framework.
* __Color appearance models and image appearance models are used to convert between different viewing conditions.__
	* This is used for device-independent imaging since it lets us translate between different color naming and color order systems.
	* E.g. SWOP CMYK → CIE XYZ → CIE L\*a\*b\* → CIE XYZ → Adobe RGB.

## Overview

### Color

Color is a an aspect of visual perception which arises from the [visual system](../../Sensory Systems/Visual System/Visual System.md).

While it’s common to say that light or objects are a certain color, color is a psychological phenomena instead of a physical property.

Many of us know what color is but it’s difficult to describe precisely without examples or circular definitions.

Most attributes of color appearance, however, can be described more exactly and are central to color research.

#### Color Appearance

Color appearance can be specified using 6 perceptual attributes which we can split into 3 related groups:

1. Hue
1. Brightness and Lightness
1. Colorfulness, Chroma, and Saturation

> ![Juxtaposed gradients depicting the different color appearance attributes.](Handbook of Color Psychology Figure 2-2.png)
>
> Visual depiction of the 6 color appearance attributes[^handbook_of_color_psychology_figure_2.2].

In mathematical terms, we can view the perceptual attributes as the following:

```math
Hue
```

```math
Brightness
```

```math
Lightness = \frac{Brightness}{Brightness\ (reference\ white)}
```

```math
Colorfulness
```

```math
Chroma = \frac{Colorfulness}{Brightness\ (reference\ white)}
```

```math
Saturation = \frac{Colorfulness}{Brightness}
```

Chroma is redundant since it can be derived if the other 5 attributes are known. In other words, we only need 5 of the 6 perceptual attributes to completely specify color appearance.

In most practical color appearance applications, however, we’re only concerned with the relative attributes of hue, lightness, and chroma.

##### Hue

>__Hue__[^color_appearance_models_page_88]
>
> Attribute of a visual perception according to which an area appears to be similar to one of the colors – red, yellow, green, and blue – or to a combination of adjacent pairs of these colors considered in a closed ring.

Like color, hue is difficult, if not impossible, to define without using examples.

Hue is an interval scale, having no meaningful 0 point.

Colors without hue can be described (such as black or white), but there’s no perception that meaningfully corresponds to a hue of 0 units. Any point on the hue ring can be arbitrarily chosen as the 0 degree mark.

##### Brightness and Lightness

> __Brightness__[^color_appearance_models_page_88]
>
> Attribute of a visual perception according to which an area appears to emit, or reflect, more or less light.

> __Lightness__[^color_appearance_models_page_88]
>
> The brightness of an area judged relative to the brightness of a similarly illuminated area that appears to be white or highly transmitting.
>
> _Note: Only related colors exhibit lightness._

Brightness and lightness are ratio scales.

Lightness can be thought of as relative brightness compared to a similarly illuminated reference white after taking illumination and viewing conditions into account.

For example, a piece of white paper when viewed outside on a sunny day has high brightness and high lightness.

When viewed in a typical indoor environment, the same piece of paper would have lower brightness (since less light is reflected) but the lightness would be about the same.

##### Colorfulness, Chroma, and Saturation

> __Colorfulness__[^color_appearance_models_page_90]
>
> Attribute of a visual perception according to which the perceived color of an area appears to be more or less chromatic.
>
> _Note: For a color stimulus of a given chromaticity and, in the case of related colors, of a given luminance factor, this attribute usually increases as the luminance is raised, except when the brightness is very high._

> __Chroma__[^color_appearance_models_page_90]
>
> Colorfulness of an area as judged as a proportion of the brightness of a similarly illuminated area that appears white or highly transmitting.
>
> _Note: For given viewing conditions and at luminance levels within the range of photopic vision, a color stimulus perceived as a related color, of a given chromaticity, and from a surface having a given luminance factor exhibits approximately constant chroma for all levels of illuminance except when the brightness is very high. In the same circumstances, at a given level of illuminance, if the luminance factor increases, the chroma usually increases._

> __Saturation__[^color_appearance_models_page_91]
>
> Colorfulness of an area judged in proportion to its brightness.
>
> _Note: For given viewing conditions and at luminance levels within the range of photopic vision, a color stimulus of a given chromaticity exhibits approximately constant saturation for all luminance levels, except when brightness is very high._

Colorfulness, chroma, and saturation are ratio scales.

Like how lightness is to brightness, chroma can be thought of as relative colorfulness compared to a similarly illuminated reference white.

In addition, there’s saturation which can be thought of as relative colorfulness but compared to an area’s own brightness instead of a similarly illuminated reference white.

#### Metamers

__Metamers__ are different spectral power distributions which have the same color appearance. In other words, objects which absorb, reflect, transmit, and emit light differently can appear to be the same color.

> Printers and screens use metamerism to produce a wide range of color appearances with only a few primary inks (CMYK) or lights (RGB)!

Metamers arise from the visual system reducing the full visible spectrum into one signal per cone photoreceptor type (3 signals total in the case of typical human vision).

#### Viewing Conditions

Different viewing conditions can cause the same object to have different color appearances. Metamers under one set of viewing conditions can also be non-metameric under another.

Viewing conditions can include physical factors, such as the illumination in the scene and how a scene is projected onto the retinae, as well as psychological factors, such as visual illusions and the viewer’s expectations.

> ![Dress which looks black and blue when viewed in bright lighting conditions or white and gold in dark lighting conditions.](xkcd_dress_color_2x.png)
>
> The infamous dress which looks black and blue or white and gold depending on the viewing conditions[^dress_color].

### Color Systems and Models

Color systems and models attempt to measure color appearances.

Simple color systems specify color appearances under only one set of viewing conditions. More complex color models predict color appearances under a wide range of viewing conditions by accounting for various physical and psychological adaptations. These can be grouped into:

1. __Color Naming Systems__
	* Name arbitrarily-chosen color appearances under no well-defined or just one set of viewing conditions.
1. __Color Order Systems__
	* Order systematically-chosen color appearances under one set of viewing conditions.
1. __Color Appearance Models__
	* Predict systematically-chosen color appearances under a range of viewing conditions.
1. __Image Appearance Models__
	* Predict systematically-chosen color appearances under a wider range of viewing conditions than color appearance models.

Many color systems and models introduce their own color scales which describe different parts of the visual sensation and perception process. These can be grouped into:

1. __Spectrophotometric Color Scales__
	* Specify color appearances at the stimulus level (spectral power distributions).
1. __Colorimetric Color Scales__
	* Specify color appearances at the sensation level (photoreceptor signals).
1. __Perceptual Color Scales__
	* Specify color appearances at the perceptual level (after all physical and psychological adaptation and processing).

> ![Diagram showing how different color scales relate to each other and how they map to different parts of the visual sensation and perception process.](Color Systems and Models Overview.png)
>
> Diagram showing how different color scales relate to each other and how they map to different parts of the visual sensation and perception process. Dashed single-sided arrows represent an irreversible reduction from a higher number of dimensions to a lower number of dimensions.

#### Color Naming Systems

__Color naming systems__ use a nominal perceptual color scale which names arbitrarily-chosen color appearances under no well-defined or just one set of viewing conditions.

These systems are practical for everyday color communication but usually lack the accuracy needed for more precise color communication.

They are often physically embodied as __color collections__, a selection of color chips which correspond to the arbitrarily-chosen color appearances named by the system.

Examples include the Pantone, Toyo, Focoltone, and Trumatch color matching systems.

#### Color Order Systems

__Color order systems__ use ordinal, interval, or ratio scales which order systematically-chosen color appearances under one set of viewing conditions.

Unlike color naming systems, color order systems have strict colorimetric characterizations and encompass a much wider range of color appearances.

These systems can be grouped by which part of the visual sensation and perception process they use to order color appearances.

##### Spectrophotometric Order

Color order systems using spectrophotometric color scales order color appearance at the stimulus level.

These systems are often used by printers and displays to create color stimuli from a few color “primaries” which are specified as spectral power distributions.

##### Colorimetric Order

Color order systems using colorimetric color scales order color appearances at the sensation level.

These systems are essential since the colorimetric color scales they introduce help form a bridge between stimulus and perception.

The most commonly used systems are from the CIE 1931 and CIE 2006 technical reports. These reports introduced two paramount colorimetric color scales: the CIE 1931 color space (a.k.a. the CIE XYZ color space) and the CIE 2006 color space (a.k.a. the CIE LMS color space).

> ![Diagram showing the CIE XYZ and LMS color spaces in relation to the visual sensation and perception process.](CIE Color Systems.png)
>
> Diagram showing how the CIE XYZ and LMS colorimetric color order scales map to the visual sensation and perception process.

Many other colorimetric color scales like CMYK-based and RGB-based ones are derived from the CIE XYZ and LMS color spaces.

> ![Diagram showing the SWOP CMYK, sRGB, AdobeRGB, DCI-P3, and ITU-R BT.2020/2100 color spaces in relation to the visual sensation and perception process.](CMYK-Based and RGB-Based Color Systems.png)
>
> Diagram showing how CMYK-based and RGB-based colorimetric color order scales map to the visual sensation and perception process.

###### CIE XYZ

The CIE 1931 technical report introduced the CIE 2° standard observer. This observer was intended to represent a typical person’s photoreceptor responses in the central 2° of their field of view.

Data for this standard observer was obtained by having people match colors through a hole that limited them to the central 2° of their field of view.

> In 1964, the CIE also introduced a CIE 10° standard observer which was intended to represent a typical person’s photoreceptor responses in the central 10° of their field of view but excluding the central 2°. This is where the central fovea is located and is intentionally ignored to remove the influence of the macula absorbing short-wavelength light (blue and violet).
>
> The CIE recommends the 10° standard observer over the 2° but the latter is still used in many models.

This metameric match data was then used to derive spectral responses for 3 imaginary photoreceptors denoted X, Y, and Z. The equations representing these photoreceptor spectral responses are known as __color matching functions__. The color scale that uses these functions as coordinates is the __CIE XYZ color space__.

It’s important to note that this was done before the structure of the human retina and cone photoreceptor spectral responses were known. Despite this, the CIE 2° standard observer and these color matching functions have worked remarkably well and are still commonly used in color appearance modelling.

###### CIE LMS

The CIE 2006 technical report introduced a new set of color matching functions which represent the spectral responses of the now known L, M, and S cone photoreceptors in the human retina. The color scale using these functions as coordinates is the __CIE LMS color space__ and is preferred over the CIE XYZ color space. There’s a conversion we can use to translate between these color spaces.

###### CMYK-Based

CMYK-based color systems are commonly used by printing standards to specify color stimuli by combining varying amounts of cyan, magenta, yellow, and black ink.

The __Specifications for Web Offset Publishing (SWOP) CMYK color space__ includes a standardized colorimetric characterization and is one of the mostly widely used CMYK-based color systems.

###### RGB-Based

RGB-based color systems are commonly used in display standards to specify color stimuli by combining varying amounts of red, green, and blue light.

The __International Telecommunication Union - Recommendation BT.709 (ITU-R BT.709) color space__ was introduced in 1993 and includes a colorimetric characterization. This color space was the standard for high definition television (HDTV) broadcast.

The __Standard RGB (sRGB) color space__ was introduced by HP and Microsoft in 1996 and then standardized by the IEC in 1999. Its colorimetric characterization is slightly adjusted from the one in ITU-R BT.709 and is the standard color space on the web.

Neither ITU-R BT.709 nor sRGB are able to fully encapsulate the full color gamut of the SWOP CMYK color space. To address this, Adobe introduced the __Adobe RGB (1998) color space__ which allows displays and projectors capable of displaying this color space to reproduce most of the SWOP CMYK color space gamut.

Another RGB-based color space which has become more prevalent is the __Digital Cinema Initiative Profile 3 (DCI-P3) color space__. This color space was introduced in 2010 and is larger than both ITU-R BT.709 and sRGB like Adobe RGB (1998) but expands more into the reds and yellows instead of the cyans and greens.

Its successor is the much larger __ITU-R BT.2020 color space__. This color space is the standard for ultra-high definition television (UHDTV) broadcast. It’s re-used in ITU-R BT.2100 which is the standard for high dynamic range (HDR) television broadcast.

###### What about HSL, HSV/HSB, YC<sub>B</sub>C<sub>R</sub>, and YP<sub>B</sub>P<sub>R</sub>?

HSL, HSV/HSB, and YC<sub>B</sub>C<sub>R</sub> color systems are transformations of RGB coordinates. Their coordinates don’t have any inherent meaning unless combined with a colorimetric characterization.

HSL and HSV/HSB are often used on the web with the sRGB color space. YC<sub>B</sub>C<sub>R</sub> and YP<sub>B</sub>P<sub>R</sub> are often used in television broadcast with the ITU-R BT.709 color space.

##### Perceptual Order

Color order systems which use perceptual color scales order color at the perceptual level.

They are often embodied by __color atlases__, a selection of color chips which correspond to the systematically-chosen color appearances specified by the system. These atlases are meant to be viewed under strictly defined viewing conditions.

These systems have been replaced by more advanced color models which can predict color appearance under a range of viewing conditions instead of just one. They are still useful, however, as they can be used to test the performance of these more advanced color models.

> ![Diagram showing the Munsell and Swedish Natural Color System (NCS) color spaces in relation to the visual sensation and perception process.](Munsell and Swedish Natural Color Systems.png)
>
> Diagram showing how perceptual color order scales map to the visual sensation and perception process.

###### Munsell Color System

The __Munsell Color System__ was created by Albert H. Munsell in the early 1900s and then renotated in the 1940s. It specifies color appearance according to hue (H), value (V), and chroma (C). The definitions used by Munsell match the modern definitions of color attributes, with value referring to lightness.

Munsell developed this system as a way to teach his students about color appearance.

###### Swedish Natural Color System (NCS)

The __Natural Color System (NCS)__ was developed by Sweden as a national standard. It specifies color appearance according to blackness (s), chromaticness (c), and hue.

The system is taught at a young age and is widely used for color communication in Sweden.

#### Color Appearance Models

Unlike color naming systems and color order systems which specify color appearances under just one set of viewing conditions, color appearance models predict color appearances under a range of viewing conditions.

To do this, they take several physical and psychological adaptation mechanisms into account such as:

* Light, dark, and chromatic adaptations
* Pupil dilation and constriction
* Receptor gain control
* Subtractive mechanisms
* High-level adaptation

More formally, the CIE Technical Committee 1-34 defined color appearance models in 1995[^testing_color_appearance_models] which Mark D. Fairchild paraphrased as the following:

> __Color Appearance Models__[^color_appearance_models_page_199]
>
> A color appearance model is any model that includes predictors of at least the relative color appearance attributes of lightness, chroma, and hue.

Fairchild notes that “[f]or a model to include reasonable predictors of these attributes, it must include at least some form of a chromatic adaptation transform. Models must be more complex to include predictors of brightness and colorfulness or to model other luminance-dependent effects such as the Stevens effect or the Hunt effect.”

In general, color appearance models try to isolate color from all other parts of visual perception. As a result, these models don’t account for spatial and temporal contrast sensitivity. Instead, they usually assume that stimuli occupy the entire field of view of the observer.

> ![Diagram showing color appearance models in relation to the visual sensation and perception process.](Color Appearance Models.png)
>
> Diagram showing how color appearance models map to the visual sensation and perception process.

###### CIELAB and CIELUV

__CIELAB__ and __CIELUV__ are color appearance models introduced by the CIE in 1976.

While these were explicitly introduced as just color spaces instead of color appearance models, both predict lightness, chroma, and hue which passes the CIE’s 1995 definition of color appearance models.

CIELAB introduces the __CIE L\*a\*b\* color space__ which can be converted to the __CIE L\*C\*<sub>ab</sub>h<sub>ab</sub> color space__, a cylindrical color space whose dimensions are lightness (L*), chroma (C*<sub>ab</sub>), and hue angle (h<sub>ab</sub>).

CIELUV introduces the __CIE L\*u\*v\* color space__ which can be converted to the __CIE L\*C\*<sub>uv</sub>h<sub>uv</sub> color space__, equivalent to how CIE L\*C\*<sub>ab</sub>h<sub>ab</sub> is to CIE L\*a\*b\*.

These color spaces are intended to be __uniform color spaces (UCS)__, where the Euclidean (straight-line) distance between two points is equal to their perceptual difference. The feasibility of a UCS, however, is questionable as it’s generally recognized that perceived color differences don’t scale linearly with changes in color appearance attributes.

The chromatic adaptation transforms for both models have issues which can lead to inaccurate color appearance predictions. CIELUV in particular can predict impossible colors and negative stimuli which have eliminated it from any serious consideration as a good color appearance model.

###### CIECAM97s, CIECAM02, and CAM16

__CIECAM97s__ is a color appearance model introduced by the CIE in 1997 in response to industry interest in a single standardized color appearance model. It was able to predict a wide range of phenomena but can be too complex for some applications.

Due to the intense focus CIECAM97s received from researchers and engineers, limitations in the model and significant improvements were quickly uncovered alongside the creation of additional data for testing newer models.

These improvements were incorporated into __CIECAM02__ which was introduced by the CIE in 2002. Compared to its predecessor, CIECAM02 is simpler yet provides better predictions. It also introduces the __CAM02-UCS__ which attempts to adjust CIECAM02’s color appearance dimensions to create a UCS.

The CIE is working on a successor to CIECAM02 currently named __CAM16__ which was published in 2017 but hasn’t been made an official standard yet. Like CIECAM02, it also introduces its own UCS named __CAM16-UCS__.

###### IPT and IC<sub>T</sub>C<sub>P</sub>

__IPT__ is a color appearance model introduced by Fritz Ebner and Mark D. Fairchild in 1998.

Unlike the previously mentioned models which try though somewhat fall short of being perceptually uniform in all color appearance dimensions, the IPT color space was designed specifically to be perceptually uniform for hue and isolate it from other color appearance attributes. This property is important for gamut mapping in image processing which is often used when a device is unable to display the full color gamut of an image. The model, however, accounts for fewer visual adaptations than more comprehensive models like CIECAM02. Regardless, this model’s exceptional hue uniformity warrants interest and is used as a basis in some more complex color models.

__IC<sub>T</sub>C<sub>P</sub>__, a modification of IPT designed by Dolby Laboratories, is part of ITU-R BT.2100 and introduces a more efficient color space than in prior ITU recommendations for image processing and broadcast use.

#### Image Appearance Models

While color appearance models have been fairly successful in describing simple stimuli, they aren’t equipped to deal with more complex stimuli like images. Image appearance models extend color appearance models to include additional parts of human vision such as spatial and temporal contrast sensitivity.

These models are still in their infancy but are an active area of research due to their applicability in computer vision and other complex imaging applications.

> ![Diagram showing image appearance models in relation to the visual sensation and perception process.](Image Appearance Models.png)
>
> Diagram showing how image appearance models map to the visual sensation and perception process.

###### S-CIELAB

__Spatial CIELAB (S-CIELAB)__ is an image appearance model developed by Xuemei Zhang and Brian A. Wandell in 1996.

The model effectively combines CIELAB with a spatial filtering step to blur away fine details that can’t be perceived. Each pixel in an image is first converted to CIE XYZ coordinates and then blurred according to the image viewing distance.

###### iCAM Framework

The __Image Color Appearance Model (iCAM) framework__ is an image appearance model developed by Mark D. Fairchild and Garrett M. Johnson in 2004.

The framework describes a way to feed differently processed versions of an image into various adaptation transforms that are chained together. Its modular nature makes it a useful blueprint for future image appearance models.

### Device-Independent Imaging

Printers and displays often use different sets of color “primaries” to display color. These create spectrophotometric color scales which are intended to be seen under different viewing conditions.

To convert between two different spectrophotometric color scales, most image processing systems will convert these coordinates into a perceptual color scale to translate between viewing conditions and then back to another spectrophotometric color scale of choice.

For example, to convert from SWOP CMYK to Adobe RGB, an image processing system may translate color coordinates through the following color spaces:

1. SWOP CMYK
1. CIE XYZ
1. CIE L\*a\*b\*
1. CIE XYZ
1. Adobe RGB

> ![Diagram showing the translation from the SWOP CMYK color space to the Adobe RGB color space.](Device Independent Imaging.png)
>
> Diagram showing the translation from the SWOP CMYK color space to the Adobe RGB color space.

## Further Learning

* [_Color Appearance Models (3rd Edition)_](https://doi.org/10.1002/9781118653128) by Mark D. Fairchild.
	* A much more technical overview of the visual system and color appearance models (this article is a simplified version of this textbook).
	* A 4th edition is planned for 2024.
* [_Handbook of Color Psychology_](https://doi.org/10.1017/CBO9781107337930) by Andrew J. Elliot, Mark D. Fairchild, and Anna Franklin.
* [_Color Ordered: A Survey of Color Order Systems from Antiquity to the Present_](https://doi.org/10.1093/acprof:oso/9780195189681.001.0001) by Rolf G. Kuehni and Andreas Schwarz.
	* A comprehensive history of color systems.
* [_Encoding high dynamic range and wide color gamut imagery_](https://dx.doi.org/10.18419/opus-9664) by Jan Fröhlich.
	* An overview and comparison of different encoding strategies for high dynamic range (HDR) and wide color gamut (WCG) imagery. Introduces IC<sub>T</sub>C<sub>P</sub>.

---

[^handbook_of_color_psychology_figure_2.2]: Figure 2.2 from [_Handbook of Color Psychology_](https://doi.org/10.1017/CBO9781107337930) by Andrew J. Elliot, Mark D. Fairchild, and Anna Franklin.

[^color_appearance_models_page_88]: Page 88 of [_Color Appearance Models (3rd Edition)_](https://doi.org/10.1002/9781118653128) by Mark D. Fairchild.

[^color_appearance_models_page_90]: Page 90 of [_Color Appearance Models (3rd Edition)_](https://doi.org/10.1002/9781118653128) by Mark D. Fairchild.

[^color_appearance_models_page_91]: Page 91 of [_Color Appearance Models (3rd Edition)_](https://doi.org/10.1002/9781118653128) by Mark D. Fairchild.

[^dress_color]: [_Dress Color_](https://xkcd.com/1492/) by xkcd.

[^testing_color_appearance_models]: [_Testing colour-appearance models: Guidelines for coordinated research_](https://onlinelibrary.wiley.com/doi/10.1002/col.5080200410) by the CIE Technical Committee 1-34.

[^color_appearance_models_page_199]: Page 199 of [_Color Appearance Models (3rd Edition)_](https://doi.org/10.1002/9781118653128) by Mark D. Fairchild.

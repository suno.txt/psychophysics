> 🚧 Under construction.

# 🎶 Sound

An overview of perceptual models for human sound perception.

> Check out the [psychophysics overview](../../Psychophysics.md) first!

[[_TOC_]]

## Key Points

?

## Overview

* _Base definitions_
	* _Sound_
		* _Pitch_
		* _Level_
	* _Pure tones v. multitones_
* _Pure tones_
	* _Equal loudness contours_
		* _ISO 226:2003_
		* _A-weighting curve_
		* _Fletcher-Munsonn curves_
	* _Target response curves (used for speaker and headphone design)_
		* _Free-field target response curve_
		* _Diffuse-field target response curve_
		* _Harman target response curves_
* _Multitones_
	* _The phenomenon of the missing fundamental._
	* _Auditory masking._

## Further Learning

* _Equal-loudness contours (Wikipedia page?)_
* _A-weighting (Wikipedia page?)_
* _Harman target response curves paper(s)_
* _Royal Institution YouTube Video on the phenomenon of the missing fundamental_

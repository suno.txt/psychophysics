> 🚧 Under construction.

# 感 Psychophysics 

An overview of psychophysics.

[[_TOC_]]

## 〽️ Introduction

### What Is Psychophysics?

> Psychophysics is the scientific study of the relationships between the physical measurements of stimuli and the sensations and perceptions that those stimuli evoke.
>
> – Mark D. Fairchild[^color_appearance_models_page_39]

We can view how we comprehend our surroundings in 3 parts[^stages_of_evaluation]:

1. Sensing and perceiving the state of our surroundings. _What happened?_
1. Interpreting our perceptions. _What does this mean?_
1. Evaluating our interpretation. _Is this what I expect?_

Psychophysics studies the first part of this process while other behavioral sciences study the latter parts.

### Why Is It Important?

#### For Designers

To create accessible products and services intended for humans, __we need to understand what (reasonable) worst case scenarios for human sensation and perception we should account for__. Psychophysics helps us create tangible guidelines for accessible design.

For design systems, perceptual models from psychophysics help us do things like generate accessible color palettes and maintain appropriate contrast.

#### For Engineers

To efficiently capture information needed for human sensation and perception, __we need to understand what best case scenarios for human sensation and perception we should account for__. Psychophysics helps us create maximally efficient encoding techniques for lossless capture and reproduction of sensory information (e.g. noise shaping in audio, the PQ transfer function for luminance encoding in imagery) as well as lossy compression techniques which can save resources while remaining perceptually indistinguishable from lossless sources in most usage scenarios (e.g. AAC in audio, dithering and chroma subsampling in imagery).

For image processing, findings from psychophysics can help inform the design of image processing techniques. For example, convolution-based edge detection techniques used for sharpening filters in image editors and convolutional layers in neural networks for machine vision are similar the responses of ganglion cells in the human eye[^color_appearance_models_page_16].

## 📏 Scales of Measurement

We need to be careful to not apply inappropriate mathematical manipulations to the measurements we make. For example, it doesn't make any sense to average a list of color names (e.g. red, orange, yellow, green, blue, indigo, violet) or to say that 1st place is twice as good as 2nd place based on rank alone.

Different scales of measurement allow for varying mathematical operations. The most common framework was proposed by psychologist Stanley Smith Stevens and contains 4 scales that progressively increase in mathematical power: nominal, ordinal, interval, and ratio[^level_of_measurement].

> These scales have the convenient acronym of NOIR, the French word for black.

### Nominal Scale

__Nominal scales__ are scales that differentiate items simply by name, category, or other qualitative features such as:

* Genre
* Language
* Color name (e.g. red, orange, yellow, green, blue, indigo, violet)
* Sky condition (clear, mostly clear, partly clear/cloudy, mostly cloudy, cloudy)[^sky_condition]

We can group items but we can't sort, find the difference between, nor find the ratio between them.

The only valid mathematical operation is equality/membership (=, ≠, ∈, ∉) and the only valid measure of central tendency is the mode.

### Ordinal Scale

__Ordinal scales__ are scales where items can be ordered/ranked.

We can group and sort items but we can't find the difference nor ratio between them.

In addition to the ones for nominal scales, valid mathematical operations include comparison (<, >) and valid measures of central tendency include the median.

### Interval Scale

__Interval scales__ are scales where the distance, but not the ratio, between items are known such as:

* Celsius and Fahrenheit temperature scales
* Geographical coordinates
* Date from an arbitrary epoch (AD, BC, Unix time)

We can group, sort, and find the differences between items but we can't find the ratios between them (there's no absolute zero).

In addition to the ones for ordinal scales, valid mathematical operations include addition/subtraction (+, -) and valid measures of central tendency include the arithmetic mean.

### Ratio Scale

__Ratio scales__ are scales where the ratio between items are known such as:

* Mass
* Length
* Duration
* Kelvin temperature scale

We can group, sort, find the difference between, and find the ratio between items (there's an absolute zero).

In addition to the ones for ordinary scales, valid mathematical operations include multiplication/division (×, ÷) and valid measures of central tendency include the geometric and harmonic means.

## 🌌 Sensory Systems

Details with regards to the brain are omitted as they aren't particularly well understood and the classical separation of sensory processing within the brain into distinct, static cortices has fallen out of favor[^livewired].

* [Visual System](Sensory Systems/Visual System/Visual System.md) (🔍 Pending review)
* [Auditory System](Sensory Systems/Auditory System/Auditory System.md) (🚧 Under construction)
* [Olfactory System](Sensory Systems/Olfactory System/Olfactory System.md) (🚧 Under construction)
* [Gustatory System](Sensory Systems/Gustatory System/Gustatory System.md) (🚧 Under construction)
* [Vestibular System](Sensory Systems/Vestibular System/Vestibular System.md) (🚧 Under construction)
* [Somatosensory System](Sensory Systems/Somatosensory System/Somatosensory System.md) (🚧 Under construction)

## 🔭 Perceptual Models

Psychophysicists create perceptual models to describe and predict human perceptions in response to arbitrary stimuli.

__It's important to keep in mind that these models are general approximates__. Creating models that can account for every individual difference often leads to very complex, impractical models and the arduous experimental process limits the amount of people that researchers can collect data from.

Some current models are also known to sharply diverge from experimental observations in some situations and none are equipped to deal with exceptional situations such as synesthesia[^synesthesia].

__Despite these shortcomings, many of these models are still useful__ as they are relatively accurate in many typical situations and provide us with a baseline to work with.

### Single System Perceptions

* Visual
	* [Color](Perceptual Models/Color/Color.md) (Hue, Brightness + Lightness, and Colorfulness + Chroma + Saturation) (🔍 Pending review)
	* Depth
	* Movement
	* Line Orientation
* Auditory
	* [Sound](Perceptual Models/Sound/Sound.md) (Pitch + Level) (🚧 Under construction)
	* [Sound Location](Perceptual Models/Sound Location/Sound Location.md) (🚧 Under construction)
* Olfactory
	* Smell
* Gustatory
	* Taste
* Vestibular
	* Head Inertia
* Somatosensory
	* Touch
	* Temperature
	* Pain
	* Posture
	* Pruritus
	* Viscera

### Multiple System Perceptions

* Balance (Vestibular + Somatosensory)
* Flavor (Olfactory + Gustatory + Somatosensory)

---

[^color_appearance_models_page_39]: Page 39 of [_Color Appearance Models (3rd Edition)_](https://doi.org/10.1002/9781118653128) by Mark D. Fairchild.

[^stages_of_evaluation]: The _Stages of Evaluation_ in Don Norman's [_Seven Stages of Action_](https://en.wikipedia.org/wiki/Seven_stages_of_action).

[^color_appearance_models_page_16]: Page 16 of [_Color Appearance Models (3rd Edition)_](https://doi.org/10.1002/9781118653128) by Mark D. Fairchild.

[^level_of_measurement]: [_Level of measurement_](https://en.wikipedia.org/wiki/Level_of_measurement) on Wikipedia.

[^sky_condition]: [_Sky Condition_](https://forecast.weather.gov/glossary.php?word=Sky%20Condition) from the National Weather Service Glossary.

[^livewired]: Talks by David Eagleman on his book [_Livewired: The Inside Story of the Ever-Changing Brain_](https://en.wikipedia.org/wiki/Livewired_(book)) at [The Royal Institution of Great Britain](https://www.youtube.com/watch?v=3rfbLZi4Eak) and [Talks at Google](https://www.youtube.com/watch?v=XwtBlh2Om18).

[^synesthesia]: [_What Is It Like To Have Synesthesia_](https://www.youtube.com/watch?v=ZVC3E16FCrk) talk by Jamie Ward at The Royal Institution of Great Britain.